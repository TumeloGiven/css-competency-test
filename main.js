import './style.css'

var obj = {"items":[
    {
        "Name":"AM/PM Plumbers", 
        "Category":"Plumbing", 
        "Tell":"011 768 0204",
        "Email":"ampm@outlook.com", 
        "Satus":"active"
    },
    {
        "Name":"Big Joe’s Plumbing and Construction", 
        "Category":"Plumbing, Construction", 
        "Tell":"011 768 0204",
        "Email":"ampm@outlook.com", 
        "Satus":"idle"
    },
    {
        "Name":"A1 Plumbing", 
        "Category":"Plumbing", 
        "Tell":"011 768 0204",
        "Email":"ampm@outlook.com", 
        "Satus":"busy"
    },
    {
        "Name":"OneTime Installations", 
        "Category":"Plumbing", 
        "Tell":"011 768 0204",
        "Email":"ampm@outlook.com", 
        "Satus":"idle"
    },
    {
        "Name":"K & P Electrical", 
        "Category":"Electrical", 
        "Tell":"011 768 0204",
        "Email":"ampm@outlook.com", 
        "Satus":"idle"
    },
    {
        "Name":"ZAP Electric", 
        "Category":"Electrical", 
        "Tell":"011 768 0204",
        "Email":"ampm@outlook.com", 
        "Satus":"inactive"
    },
    {
        "Name":"Mr Gutter", 
        "Category":"Roofing", 
        "Tell":"011 768 0204",
        "Email":"ampm@outlook.com", 
        "Satus":"active"
    },
    {
        "Name":"Sparkle Pools", 
        "Category":"Pools", 
        "Tell":"011 768 0204",
        "Email":"ampm@outlook.com", 
        "Satus":"idle"},

]}

var loadContent = function() {
    
    var mydata = JSON.parse(JSON.stringify(obj));

    var content = document.getElementById('app-list-item');
    var string = '';
    var _length = Object.keys(obj.items).length;
    
    for(var i = 0;i < _length; i++)
    {
        var tag = document.createElement("ul");
        tag.setAttribute('id', 'app-list-item');
        tag.setAttribute('class', 'tg-service-provider-list');

        string = string + '<li class="tg-list-item tg-status-'+obj.items[i].Satus+'">'+
        '<div class="tg-list-item-content  tg-float-left"> <div class="tg-sp-gn-info tg-float-left tg-mb-float-none mb-width-100 width-45"><p class="tg-text-cm-name tg-color-white">'+obj.items[i].Name+'</p> <p class="tg-text-cm-cat tg-color-ghost">'+ obj.items[i].Category +'</p> </div>'+
          '<div class="tg-sp-cn-info tg-float-left mb-width-100 width-45"> <p class="tg-text-cm-tell tg-color-white mb-width-100 width-50 tg-mb-float-none tg-float-left"><svg xmlns="http://www.w3.org/2000/svg" id="Raw" viewBox="0 0 256 256"><rect fill="none" width="256" height="256" /><path fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" d="M 92.4763 124.815 a 84.3478 84.3478 0 0 0 39.0533 38.8759 a 7.92754 7.92754 0 0 0 7.8287 -0.59231 L 164.394 146.405 a 8 8 0 0 1 7.58966 -0.69723 l 46.837 20.073 A 7.97345 7.97345 0 0 1 223.619 174.077 A 48.0088 48.0088 0 0 1 176 216 A 136 136 0 0 1 40 80 A 48.0088 48.0088 0 0 1 81.923 32.381 a 7.97345 7.97345 0 0 1 8.29668 4.79823 L 110.31 84.0571 a 8 8 0 0 1 -0.65931 7.53226 L 93.0145 117.009 A 7.9287 7.9287 0 0 0 92.4763 124.815 Z" /></svg>'+
              '<span>'+ obj.items[i].Tell +'</span></p>'+
            '<p class="tg-text-cm-email tg-color-blue  mb-width-100 width-50 tg-float-left tg-mb-float-none"><svg xmlns="http://www.w3.org/2000/svg" id="Raw" viewBox="0 0 256 256"><rect fill="none" width="256" height="256" /><path fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" d="M 32 56 H 224 a 0 0 0 0 1 0 0 V 192 a 8 8 0 0 1 -8 8 H 40 a 8 8 0 0 1 -8 -8 V 56 A 0 0 0 0 1 32 56 Z" /><polyline fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" points="224,56 128,144 32,56" /></svg>'+
              '<span>'+ obj.items[i].Email +'</span></p></div></div>'+
              '<div class="tg-sp-action-btn tg-float-left">'+
              ' <button class="tg-action-btn tg-btn-edit">'+
              ' <svg xmlns="http://www.w3.org/2000/svg" id="Raw" viewBox="0 0 256 256">'+
              ' <rect fill="none" width="80%" height="100%" />'+
              ' <path fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" d="M 96 216 H 48 a 8 8 0 0 1 -8 -8 V 163.314 a 8 8 0 0 1 2.34315 -5.65686 l 120 -120 a 8 8 0 0 1 11.3137 0 l 44.6863 44.6863 a 8 8 0 0 1 0 11.3137 Z" />'+
              ' <line fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" x1="136" y1="64" x2="192" y2="120" />'+
              ' <polyline fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" points="216,216 96,216 40.509,160.509" />'+
              ' </svg>'+
              ' </button>'+
              ' <button class="tg-action-btn tg-btn-delete">'+
              '<svg xmlns="http://www.w3.org/2000/svg" id="Raw" viewBox="0 0 256 256">'+
              '<rect fill="none" width="256" height="256" />'+
              '<line fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" x1="215.996" y1="56" x2="39.9961" y2="56" />'+
              '<line fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" x1="104" y1="104" x2="104" y2="168" />'+
              ' <line fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" x1="152" y1="104" x2="152" y2="168" />'+
              ' <path fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" d="M 200 56 V 208 a 8 8 0 0 1 -8 8 H 64 a 8 8 0 0 1 -8 -8 V 56" />'+
              '<path fill="none" stroke="#676767" stroke-linecap="round" stroke-linejoin="round" stroke-width="16" d="M 168 56 V 40 a 16 16 0 0 0 -16 -16 H 104 A 16 16 0 0 0 88 40 V 56" />'+
              '</svg>'+
            '</button>'+
          '</div>'+
      '</li>';
    }

    tag.innerHTML = string;
    
    return tag;
}

document.querySelector('#app-content').appendChild(loadContent());
